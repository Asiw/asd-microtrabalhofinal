﻿Rafael Riedel - 66984


ANÁLISE TÉCNICA: Python Flask

Micro Trabalho de conclusão da disciplina Plataformas de Produtividade no Desenvolvimento de Software.


Professor: Marco Aurélio

Belo Horizonte
Setembro - 2016





Python Flask: Um microframework para a Web


Desenvolvido com o intuito de ser simples e extensível, o Flask é um microframework que compreende apenas a camada web e nada mais. Seu uso não se restringe apenas ao desenvolvimento de API’s web, mas para qualquer aplicação que tenha como demanda uma interface de comunicação HTTP. Flask não vem carregado com nenhum outro recurso para o desenvolvimento de aplicações, pois esta é uma decisão que é deixado à cargo do desenvolvedor, como qual camada de abstração de dados, modelo de validação, sistema de template, entre outros. Para isto já existem bons frameworks em Python que podem ser facilmente acoplados na aplicação e utilizados juntamente com o Flask sem maiores complicações.


Outro detalhe é a extensibilidade do Flask, apesar de ser micro, é possível desenvolver extensões para adicionar funcionalidades ao framework sem sobrecarregá-lo com recursos desnecessários. O objetivo das extensões é ajudar o desenvolvedor executar algumas operações mais comuns, simplificando o desenvolvimento e assim aumentando a produtividade. Além disto o Flask também provê hooks no middlewares WSGI, o que permite alterar o comportamento de determinadas funcionalidades do WSGI encapsulado na aplicação.


Apesar de ser um microframework, o Flask é baseado no Wekzeug e Jinja2 frameworks. Werkzeug é um framework utilitário WSGI que realiza o parsing HTTP, trabalha com os requests e responses,com suporte Unicode, suporte a sessões, além de prover um console de debug interativo em Javascript dentro do navegador. Já o Jinja2 é um sistema de template disponível para o Python, baseado no sistema de template do Django. 


Outro ponto a se observar é que o Flask já provê um servidor HTTP para que se possa desenvolver a aplicação sem a necessidade de configurar um servidor, por exemplo, Apache. Porém devido a compatibilidade WSGI do Flask, pode-se facilmente utilizar um servidor HTTP com suporte ao WSGI ao Python.


Recursos
Flask provê para o desenvolvedor todo um conjunto de funcionalidades para criar aplicações web de forma prática e rápida. O sistema de roteamento permite criar URLs elegantes em apenas uma linha de código, onde pode-se definir além do caminho e método, o tipo do dado esperado, assim é realizada a validação da URL pelo tipo de dado informado, podendo retornar 404 caso não seja o esperado. Uma característica da definição das URLs é o comportamento definido com o uso da “/” final. Caso a URL tenha uma barra no final, o Flask irá tratar como se fosse uma pasta de sistema, o que se acessar com ou sem a barra, ele irá redirecionar para a URL com a “/” no final. Já uma URL definida sem a barra, ela se comporta como se fosse um arquivo, acessando esta URL com a barra, produzirá o erro 404 Not Found. As URLs também podem ser geradas, ao invés de especificar a URL estaticamente, assim com o comando url_for, basta informar o nome do método com os parâmetros que a URL é gerada.


Os arquivos estáticos recomenda-se utilizar um servidor HTTP para provê-los, entretanto para o ambiente de desenvolvimento o Flask provê estes arquivos, bastando criar o caminho /static.


Conforme especificado anteriormente, a renderização dos templates é feito através do Jinja2. Tudo o que se precisa para renderizar é usar o método render_template informando o nome do arquivo e os parâmetros para serem repassados ao templates. Os templates ficam hospedados dentro do caminho /templates, que pode ser um módulo ou um pacote. Dentro dos templates é possível ter acesso aos objetos de request, session e g (objetos que armazenam valores de desejo do desenvolvedor). O template Jinja2 fornece todos os recursos de programação e formatação para os valores, além de ser possível de trabalhar com herança entre os templates.


No Flask as informações de request são providas pelo objeto global request. Através deste objeto é possível acessar os dados de formulário enviados através de POST ou PUT pelo atributo form, os valores enviados através da URL através do atributo args, dados de headers, cookies, uploads de arquivos, entre outros. O Flask trata qualquer valor retornado por um método como um objeto response, assim uma string será devidamente convertida para um response.


As sessões permite acessar os dados do usuário entre um request e outro, este recurso é implementado em cima dos cookies, e estes são devidamente criptografados, assim mesmo que o usuário veja o conteúdo do cookie, ele não pode modificar (ao menos que possua a chave utilizada para a assinatura).


Outro recurso interessante do Flask é o “Message Flashing”, que permite retornar para o usuário o feedback de um request. É um mecanismo muito simples que permite dar ao usuário um retorno através de um sistema de flashing, que é basicamente armazenar uma mensagem ao final do request e acessá-la no template para expor a mensagem.


Flask também possui um conjunto de recursos para realizar o tratamento de erros na aplicação. Para isto é possível contar com hooks que são chamados quando houver algum bad request (erros 400). Também pode-se realizar o tratamento de exceções, assim consegue-se capturar o erro e todos os dados envolvidos no request, para que seja avaliado posteriormente. Os erros podem ser capturados pelo log para um arquivo, como também ser enviados para um e-mail, de forma que o desenvolvedor ou a equipe de suporte seja notificada assim que este ocorra. A equipe de desenvolvimento do Flask recomenda o uso do Sentry para o monitoramento dos erros de aplicação, assim é possível ter acesso aos dados do request, stacktrace, e até mesmo as variáveis locais.


O Flask também provê um debugger out of the box no navegador, onde é possível ver o stacktrace e até mesmo um console para inspecionar as variáveis. Entretanto é possível conectar um debugger remoto, como o Aptana do Eclipse.


Demonstração


Segue abaixo o código mínimo que demonstra o funcionamento de uma aplicação em Flask


from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
   return 'Hello, World!'


Casos de usos
http://sopython.com/


https://www.rockyou.fm/


http://dotshare.it/




Além destes exemplos, já desenvolvi uma aplicação para automação comercial de terminais de pagamento remoto (PayGo), onde a interface de comunicação e de administração foi desenvolvida inteiramente em Flask.


Livros e sites de aprendizado


http://flask.pocoo.org/
Site mais importante do Flask, o próprio site do Flask. Nele encontra-se uma documentação extensiva sobre todos os recursos do Flask.


Flask Web Development
Miguel Grinberg
O’Reilly Media
2014


Canal IRC #pocoo em irc.freenode.net
